﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour, IUseable
{
    public PlayerMovement pmScript;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Use()
    {
        //Debug.Log("Used Ladder");
        if (pmScript.OnLadder)
        {
            UseLadder(false,3);
        }
        else
        {
            UseLadder(true,0);
        }
        
    }
    private void UseLadder(bool onLadder, int gravity)
    {
        pmScript.OnLadder = true;
        pmScript.rB2D.gravityScale = gravity;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            UseLadder(false, 3);
        }
    }
}
