﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public Rigidbody2D rB2D;
    public float runSpeed;
    public float jumpForce;
    public SpriteRenderer spriteRenderer;
    public Animator animator;
    public bool OnLadder;
    public float climbSpeed;
    private IUseable useable;

    // Start is called before the first frame update
    void Start()
    {
        OnLadder = false;
        rB2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if(Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
    }
    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if(rB2D.velocity.x > 0)
        {
            spriteRenderer.flipX = false;
        }else
        if(rB2D.velocity.x <0)
        {
            spriteRenderer.flipX = true;
        }

        if(Mathf.Abs(horizontalInput) > 0f)
        {
            animator.SetBool("isWalking", true);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }
        if (OnLadder)
        {
            rB2D.velocity = new Vector2(horizontalInput * climbSpeed, verticalInput * climbSpeed);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            Use();
        }
    }

    private void Use()
    {
        if(useable != null)
        {
            useable.Use();
        }
    }

    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Useable")
        {
            useable = other.GetComponent<IUseable>();
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "Useable")
        {
            useable = null;
            OnLadder = false;
        }
    }
}