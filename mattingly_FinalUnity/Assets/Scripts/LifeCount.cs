﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LifeCount : MonoBehaviour
{
    public TextMeshProUGUI lifeCountText;
    public int lifeCount;
    public Rigidbody2D rB2D;

    //public bool m_IsPlayerCaught;

    private void Start()
    {
        lifeCount = 3;

        SetLifeCountText();
        rB2D = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Enemy")
        {
            lifeCount -= 1;
            //m_IsPlayerCaught = true;
            
            SetLifeCountText();
            if(rB2D.position.y > 10 && rB2D.position.y < 27)
            {
                transform.position = new Vector3(6f, 11f, 0f);
            }
            else if (rB2D.position.y > 28)
            {
                transform.position = new Vector3(-3f, 29f, 0f);
            }
            else
            {
                transform.position = new Vector3(10f, 0f, 0f);
            }
        }
    }

    public void SetLifeCountText()
    {
        lifeCountText.text = "Lives Remaining:" + lifeCount.ToString();
    }
}
