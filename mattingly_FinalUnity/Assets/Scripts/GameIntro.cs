﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameIntro : MonoBehaviour
{
    public GameObject introBackgroundImageCanvasGroup;
    private bool isShowing;
    // Start is called before the first frame update
    void Start()
    {
        isShowing = true;
        introBackgroundImageCanvasGroup.SetActive(isShowing);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Destroy(gameObject);
        }
    }
}
