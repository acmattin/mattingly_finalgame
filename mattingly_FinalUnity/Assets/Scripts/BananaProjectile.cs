﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BananaProjectile : MonoBehaviour
{
    public float speed = 4f;
    public int damage = 10;
    public Rigidbody2D rb;
    public float m_Lifespan = 1f;
    public PlayerMovement pmScriptRef;
    // Start is called before the first frame update
    void Start()
    {
        pmScriptRef = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
        if(pmScriptRef.spriteRenderer.flipX == true)
        {
            rb.velocity = transform.right * speed * -1;
        }
        else
        {
            rb.velocity = transform.right * speed;
        }
        
       
        Destroy(gameObject, m_Lifespan);

    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        EnemyHealth enemy = hitInfo.GetComponent<EnemyHealth>();
        if(enemy != null)
        {
            enemy.TakeDamage(damage);
            Destroy(gameObject);
        }
        Debug.Log(hitInfo.name);
    }
}
