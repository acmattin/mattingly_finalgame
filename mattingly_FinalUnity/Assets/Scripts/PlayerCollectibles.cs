﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerCollectibles : MonoBehaviour
{
    //private float banana = 0;
    public TextMeshProUGUI countText;

    public int count;

    private void Start()
    {
        count = 0;

        SetCountText();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.transform.tag == "BananaPickup")
        {
            Destroy(other.gameObject);
            count += 1;
            SetCountText();
        }
    }

    public void SetCountText()
    {
        countText.text = "Bananas:" + count.ToString();
    }
}
