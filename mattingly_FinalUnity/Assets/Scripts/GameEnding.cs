﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public LifeCount lcScriptRef;

    bool m_IsPlayerAtExit;
    float m_Timer;

    private void Start()
    {
        lcScriptRef = GameObject.FindWithTag("Player").GetComponent<LifeCount>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }

    void Update()
    {
        if (m_IsPlayerAtExit)
        {
            EndLevel();
        }
        if(lcScriptRef.lifeCount == 0)
        {
            RestartLevel();
        }
        //if (lcScriptRef.m_IsPlayerCaught)
        //{
           // RestartLevel();
        //}
    }

    void EndLevel()
    {
        m_Timer += Time.deltaTime;

        exitBackgroundImageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            Application.Quit();
        }
    }
    void RestartLevel()
    {
        m_Timer += Time.deltaTime;

        caughtBackgroundImageCanvasGroup.alpha = m_Timer / fadeDuration;
        //player.transform.position = new Vector3(10f, 0f, 0f);
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            Application.Quit();
        }
    }
}
